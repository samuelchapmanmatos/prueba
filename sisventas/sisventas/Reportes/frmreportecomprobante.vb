﻿Public Class frmreportecomprobante

    Private Sub frmreportecomprobante_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            'TODO: This line of code loads data into the 'dbventasDataSet.generar_comprobante' table. You can move, or remove it, as needed.
            Me.generar_comprobanteTableAdapter.Fill(Me.dbventasDataSet.generar_comprobante, idventa:=txtidventa.Text)

            Me.ReportViewer1.RefreshReport()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Me.ReportViewer1.RefreshReport()
        End Try
        
    End Sub
End Class